import template from "./sw-order-detail-base.html.twig";

const {Criteria} = Shopware.Data;

Shopware.Component.override('sw-order-detail-base', {
    template
});
