import template from './sw-order-line-items-grid.html.twig';
import './sw-order-line-items-grid.scss';

const { Component } = Shopware;

Component.override('sw-order-line-items-grid', {
    template,

    inject: ['acl'],

    data() {
        return {
            isLoading: false,
            selectedItems: {},
            searchTerm: ''
        };
    },
    computed: {
        getLineItemColumns() {
            const columnDefinitions = [{
                property: 'productImage',
                dataIndex: 'productImage',
                label: 'Image',
                width: '90px'
            },{
                property: 'pNumber',
                dataIndex: 'sw-order.detailBase.columnArtNo',
                label: 'ArtNo',
                width: '90px'
            }, {
                property: 'label',
                dataIndex: 'label',
                label: 'sw-order.detailBase.columnProductName',
                allowResize: false,
                primary: true,
                inlineEdit: true,
                width: '200px'
            },{
                property: 'unitPrice',
                dataIndex: 'unitPrice',
                label: this.unitPriceLabel,
                allowResize: false,
                align: 'right',
                inlineEdit: true,
                width: '120px'
            }, {
                property: 'quantity',
                dataIndex: 'quantity',
                label: 'sw-order.detailBase.columnQuantity',
                allowResize: false,
                align: 'right',
                inlineEdit: true,
                width: '80px'
            }, {
                property: 'totalPrice',
                dataIndex: 'totalPrice',
                label: this.taxStatus === 'net' ?
                    'sw-order.detailBase.columnTotalPriceNet' :
                    'sw-order.detailBase.columnTotalPriceGross',
                allowResize: false,
                align: 'right',
                width: '80px'
            }];

            if (this.taxStatus !== 'tax-free') {
                return [...columnDefinitions, {
                    property: 'price.taxRules[0]',
                    label: 'sw-order.detailBase.columnTax',
                    allowResize: false,
                    align: 'right',
                    inlineEdit: true,
                    width: '100px'
                }];
            }

            return columnDefinitions;
        }
    }
});
