// Order module integration
import './module/sw-order/component/sw-order-line-items-grid';
import './module/extension/sw-order-detail-base/index';


import deDE from './module/sw-order/snippet/de-DE.json';
import enGB from './module/sw-order/snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
